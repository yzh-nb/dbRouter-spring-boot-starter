package com.yzh.dbrouter.annotation;

import java.lang.annotation.*;

/**
 * 路由注解
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DBRouter {
    /**
     * 分库分表字段
     * @return
     */
    String key() default "";
}
