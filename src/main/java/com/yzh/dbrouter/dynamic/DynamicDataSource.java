package com.yzh.dbrouter.dynamic;

import com.yzh.dbrouter.DBContextHolder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * 动态数据源类
 */
public class DynamicDataSource extends AbstractRoutingDataSource {

    @Override
    protected Object determineCurrentLookupKey() {
        //获取 路由设置到ThreadLocal的结果
        String key = DBContextHolder.getDbKey();
        return "db" + key;
    }
}
