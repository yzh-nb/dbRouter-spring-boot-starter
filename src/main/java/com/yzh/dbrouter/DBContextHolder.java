package com.yzh.dbrouter;

/**
 * 数据上下文，使用两个本地线程类记录分库、分表的路由结果
 */
public class DBContextHolder {
    private static final ThreadLocal<String> dbKey = new ThreadLocal<>();
    private static final ThreadLocal<String> tbKey = new ThreadLocal<>();

    public static void setDbKey(String dbKeyIdx) {
        dbKey.set(dbKeyIdx);
    }

    public static String getDbKey() {
        return dbKey.get();
    }

    public static void setTbKey(String tbKeyIdx) {
        tbKey.set(tbKeyIdx);
    }

    public static String getTbKey() {
        return tbKey.get();
    }

    public static void clearDbKey() {
        dbKey.remove();
    }

    public static void clearTbKey() {
        tbKey.remove();
    }
}
